# Periodic Data

Periodic Data Service

GET ```/api/periodic```

#### Request Parameter

```date: String (YYYY-MM-DD)```

#### Request Example
```/api/periodic?date=2019-01-10```

#### Response
```
{
    "data": {
        "perOpDate": String,
        "perOpLog": Array[{
                "br": String,
                "perOpDate": String,
                "perOpType": String,
                "perOpTypeName": String,
                "phase": Int,
                "phaseName": String,
                "phaseStart": String,
                "phaseEnd": String,
                "RecCount": Int,
                "Duration": Int,
                "SLA1": Int,
                "SLA2": Int,
                "SLAResult": Int
            }]
    },
    "code": Int,
    "status": String
}
```

#### Response Example
```
{
    "data": {
        "perOpDate": "2019-01-10",
        "perOpLog": [
            {
                "br": "000001",
                "perOpDate": "2019-01-10",
                "perOpType": "000",
                "perOpTypeName": "SOD",
                "phase": 1,
                "phaseName": "SOD1 Processing",
                "phaseStart": "2019-01-10 14:07:01.960",
                "phaseEnd": "2019-01-10 14:07:06.220",
                "RecCount": 274,
                "Duration": 4260,
                "SLA1": 69,
                "SLA2": 137,
                "SLAResult": 3
            },
            {
                "br": "000001",
                "perOpDate": "2019-01-10",
                "perOpType": "000",
                "perOpTypeName": "SOD",
                "phase": 2,
                "phaseName": "SOD2 Processing",
                "phaseStart": "2019-01-10 14:07:12.523",
                "phaseEnd": "2019-01-10 14:07:12.820",
                "RecCount": 1029,
                "Duration": 296,
                "SLA1": 257,
                "SLA2": 1029,
                "SLAResult": 2
            }
        ]
    },
    "code": 200,
    "status": "OK"
}
```