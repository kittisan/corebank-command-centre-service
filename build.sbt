name := "single-file-api"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies += "io.spray" %%  "spray-json" % "1.3.5"
libraryDependencies += "com.typesafe.akka" %% "akka-http" % "10.1.5"
libraryDependencies += "com.typesafe.akka" %% "akka-http-core" % "10.1.5"
libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.4.17"
libraryDependencies += "com.typesafe.akka" %% "akka-persistence" % "2.5.21"
libraryDependencies += "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.7"

libraryDependencies += "de.heikoseeberger" %% "akka-http-circe" % "1.22.0"
libraryDependencies += "de.heikoseeberger" %% "akka-sse" % "2.0.0"
libraryDependencies += "ch.megard" %% "akka-http-cors" % "0.3.0"

val circeVersion = "0.9.1"
libraryDependencies ++= Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-parser"
).map(_ % circeVersion)

val kafkaVersion = "1.1.0"
libraryDependencies ++= Seq(
  "org.apache.kafka" % "kafka_2.12",
  "org.apache.kafka" % "kafka-streams"
).map(_ % kafkaVersion)