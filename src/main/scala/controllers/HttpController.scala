package controllers

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.BasicHttpCredentials
import akka.stream.Materializer
import configs.Config
import io.circe.generic.auto._
import io.circe.syntax._

import scala.concurrent.{ExecutionContext, Future}
import scala.language._

class HttpController(implicit sys: ActorSystem, mat: Materializer, ec: ExecutionContext) {

  case class RequestBody(runDate: String)

  def getPeriodicDataController(date: Option[String]) = {
    val requestParam = date match {
      case None => HttpEntity.Empty
      case Some(d) => {
        HttpEntity(ContentTypes.`application/json`, new RequestBody(d).asJson.noSpaces)
      }
    }

    val authUser = Config.apiAuthenticationUser
    val authPass = Config.apiAuthenticationPass

    val authorization = headers.Authorization(BasicHttpCredentials(authUser, authPass))
    val uri = Config.apiPath
    val responseFuture: Future[HttpResponse] = Http().singleRequest(
      HttpRequest(
        HttpMethods.GET,
        uri,
        headers = List(authorization),
        entity = requestParam)
    )
    responseFuture
  }
}

