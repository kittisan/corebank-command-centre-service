package configs

import com.typesafe.config.ConfigFactory

object Config {
  val config = ConfigFactory.load()
  final val serverHost = config.getString("server.host")
  final val serverPort = config.getInt("server.port")

  final val kafkaHost = config.getString("kafka.host")
  final val kafkaPort = config.getInt("kafka.port")
  final val kafkaTopic= config.getString("kafka.topic")

  final val apiPath = config.getString("api.path")
  final val apiAuthenticationUser = config.getString("api.authentication.user")
  final val apiAuthenticationPass = config.getString("api.authentication.pass")

}
