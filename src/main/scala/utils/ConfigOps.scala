package utils

import java.util.Properties

import com.typesafe.config.ConfigException._
import com.typesafe.config._

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.util.{Failure, Success, Try}

object ConfigOps {

  implicit class EnrichedConfigObject(val c: ConfigObject) extends AnyVal {
    def toProperties: Properties = {
      def toProps(m: mutable.Map[String, ConfigValue]): Properties = {
        val props = new Properties(null)
        m.foreach { case (k, cv) =>
          val v = cv match {
            case x if x.valueType() == ConfigValueType.OBJECT => toProps(x.asInstanceOf[ConfigObject].asScala)
            case x if cv.unwrapped ne null => cv.unwrapped.toString
            case _ => null
          }
          if (v ne null) props.put(k, v)
        }
        props
      }

      toProps(c.asScala)
    }

    def toPropertiesFlatPath(delimiter: String = "."): Properties = {
      def toProps(m: mutable.Map[String, ConfigValue], path: Seq[String]): Map[String, String] =
        m.foldLeft(Map.empty[String, String]) { (accum, kv) =>
          kv match {
            case (k, cv) if cv.valueType() == ConfigValueType.OBJECT => accum ++ toProps(cv.asInstanceOf[ConfigObject].asScala, path :+ k)
            case (k, cv) if cv.unwrapped ne null => accum + ((path :+ k).mkString(delimiter) -> cv.unwrapped.toString)
            case _ => accum
          }
        }

      val map = toProps(c.asScala, Seq.empty)
      val p = new Properties()
      p.putAll(map.asJava)
      p
    }
  }

  implicit class EnrichedConfig(val c: Config) extends AnyVal {
    def getStringSingleOrList(fn: String): Either[String, List[String]] = {
      val tryBoth = Try {
        Right(c.getStringList(fn).asScala.toList)
      }.recover {
        case _: WrongType => Left(c getString fn)
      }
      tryBoth match {
        case Success(ei) => ei
        case Failure(t) => throw t
      }
    }

    def getLongOption(fn: String): Option[Long] = if (c.hasPath(fn)) Some(c.getLong(fn)) else None
  }

}