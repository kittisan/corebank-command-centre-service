package models

import java.text.SimpleDateFormat
import java.util.Date

import akka.http.scaladsl.model.StatusCodes
import services.DecoderService

case class PeriodicData(perOpDate: String,
                        perOpLog: List[PerOpLog],
                        status: String)

case class PerOpLog(br: String,
                    perOpDate: String,
                    perOpType: String,
                    perOpTypeName: String,
                    phase: Int,
                    phaseName: String,
                    phaseStart: String,
                    phaseEnd: String,
                    RecCount: Int,
                    Duration: Int,
                    SLA1: Int,
                    SLA2: Int,
                    SLAResult: Int) {
  def convertToOutput = {
    new PeriodicDataOutput(getGroupDate, br, perOpDate, perOpType, perOpTypeName,
      phase, phaseName, phaseStart, phaseEnd,
      RecCount, Duration, SLA1, SLA2, SLAResult
    )
  }

  def getGroupDate = {
    val perOpDateFormat = new SimpleDateFormat("yyyy-MM-dd")
    val pdDate = perOpDateFormat.parse(perOpDate)
    perOpDateFormat.format(new Date(pdDate.getTime + (1000 * 60 * 60 * 24)))
  }

}

case class PeriodicDataOutput(groupDate: String,
                              branch: String,
                              systemDate: String,
                              perOpType: String,
                              perOpTypeName: String,
                              phase: Int,
                              phaseName: String,
                              phaseStart: String,
                              phaseEnd: String,
                              RecCount: Int,
                              Duration: Int,
                              SLA1: Int,
                              SLA2: Int,
                              SLAResult: Int)

case class PeriodicOKResponse(data: PeriodicData,
                              code: Int = StatusCodes.OK.intValue,
                              status: String = StatusCodes.OK.reason)

case class PeriodicErrorResponse(errors: String,
                                 code: Int,
                                 status: String)


