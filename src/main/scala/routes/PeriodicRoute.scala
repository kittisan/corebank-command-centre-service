package routes

import akka.actor.ActorSystem
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.Materializer
import controllers.HttpController
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._
import io.circe.generic.auto._
import models.{PeriodicData, PeriodicErrorResponse, PeriodicOKResponse}
import services.DecoderService

import scala.concurrent.ExecutionContext
import scala.language._
import scala.util.{Failure, Success}

class PeriodicRoute(implicit sys: ActorSystem, mat: Materializer, ec: ExecutionContext) {

  def staticRoute: server.Route =
    (get & path("api" / "periodic") & parameter("date".as[String].?)) { date =>
      val httpController = new HttpController
      val responseFuture = httpController.getPeriodicDataController(None)

      val periodicData = for {
        response <- responseFuture
        data <- Unmarshal(response.entity).to[PeriodicData]
      } yield (data)

      onComplete(periodicData){
        case Success(res) => complete(PeriodicOKResponse(res))
        case Failure(ex) => {
          complete(PeriodicErrorResponse(ex.getMessage, StatusCodes.InternalServerError.intValue, StatusCodes.InternalServerError.reason))
        }
      }
    }

}
