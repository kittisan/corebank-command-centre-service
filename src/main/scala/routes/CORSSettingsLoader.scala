package routes

import akka.http.scaladsl.model.headers.{HttpOrigin, HttpOriginRange}
import akka.http.scaladsl.model.{HttpMethod, HttpMethods}
import ch.megard.akka.http.cors.scaladsl.CorsDirectives
import ch.megard.akka.http.cors.scaladsl.model.HttpHeaderRange
import ch.megard.akka.http.cors.scaladsl.settings.CorsSettings
import com.typesafe.config.ConfigException._
import com.typesafe.config._
import utils.ConfigOps._

import scala.collection.JavaConverters._

object CORSSettingsLoader {

  def load(): CorsSettings = {
    CorsSettings.defaultSettings
  }

  private def getAllowedOrigins(c: Config): HttpOriginRange = c.getStringSingleOrList("allowedOrigins") match {
    case Left("*") => HttpOriginRange.*
    case Right(Nil) => throw new BadValue("allowedOrigins", "CORS allowed origin list must be non-empty. If you want to allow all, use '*' String instead")
    case Right(li) => HttpOriginRange(li.map(HttpOrigin.apply): _*)
    case Left(_) => throw new BadValue("allowedOrigins", "CORS allowed origins must be non-empty String array or '*' String value")
  }

  private def getAllowedHeaders(c: Config): HttpHeaderRange = c.getStringSingleOrList("allowedHeaders") match {
    case Left("*") => HttpHeaderRange.*
    case Right(Nil) => throw new BadValue("allowedHeaders", "CORS allowed header list must be non-empty. If you want to allow all, use '*' String instead")
    case Right(li) => HttpHeaderRange(li: _*)
    case Left(_) => throw new BadValue("allowedHeaders", "CORS allowed headers must be non-empty [String] array or '*' String value")
  }

  private def getAllowedMethods(c: Config): Seq[HttpMethod] = {
    val convResult = c.getStringList("allowedMethods").asScala.toList map (s => s -> HttpMethods.getForKey(s.trim.toUpperCase))
    val badInput = convResult.filter(kv => kv._2.isEmpty) map (_._1)
    if (badInput.nonEmpty) {
      throw new BadValue("allowedMethods", "CORS setting contains unrecognized HTTP methods: " + badInput.mkString(", "))
    }
    convResult.map(_._2).flatten
  }

  private def getMaxAge(c: Config) = c.getLongOption("maxAgeSecond")

  def rejectionHandler = CorsDirectives.corsRejectionHandler
}