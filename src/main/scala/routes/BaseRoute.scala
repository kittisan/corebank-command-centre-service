package routes

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server._
import akka.stream.Materializer
import ch.megard.akka.http.cors.scaladsl.CorsDirectives._
import ch.megard.akka.http.cors.scaladsl.settings.CorsSettings
import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport

import scala.concurrent.ExecutionContext
import scala.language._

class BaseRoute(implicit sys: ActorSystem, mat: Materializer, ec: ExecutionContext) extends FailFastCirceSupport {

  def apply(): Route = handleErrors {
    cors(corsSetting) {
      handleErrors {
        apiRoute
      }
    }
  }
  val corsSetting: CorsSettings = CORSSettingsLoader.load()
  val rejectionHandler: RejectionHandler = CORSSettingsLoader.rejectionHandler withFallback RejectionHandler.default
  val handleErrors: Directive0 = handleRejections(rejectionHandler)

  val periodicRoute = new PeriodicRoute

  private val apiRoute =
    periodicRoute.staticRoute
}
