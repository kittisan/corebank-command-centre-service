import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.directives.DebuggingDirectives
import akka.stream.ActorMaterializer
import configs.Config
import routes.BaseRoute

object MainServer {
  def main(args: Array[String]): Unit = {

    implicit val system = ActorSystem()
    implicit val materializer = ActorMaterializer()
    implicit val ec = system.dispatcher

    val route = new BaseRoute
    val routeLogged = DebuggingDirectives.logRequestResult("Client ReST", Logging.InfoLevel)(route())

    Http().bindAndHandle(routeLogged, Config.serverHost, Config.serverPort) map {
      binding =>
        println(s"REST interface bound to ${binding.localAddress}")
    } recover {
      case ex =>
        println(s"REST interface could not bind to $Config.host:$Config.port", ex.getMessage)
    }
  }

}