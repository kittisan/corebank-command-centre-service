package services

import java.util.Properties
import java.util.concurrent.Future

import configs.Config._
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord, RecordMetadata}

import scala.collection.JavaConverters._

class KafkaService(topicName: String, groupId: String) {

  val props: Properties = new Properties()
  props.put("bootstrap.servers", s"${kafkaHost}:${kafkaPort}")
  props.put("group.id", groupId)
  props.put("enable.auto.commit", "true")
  props.put("auto.offset.reset", "latest")
  props.put("session.timeout.ms", "30000")
  props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")
  props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer")

  val consumer = new KafkaConsumer[String, String](props)
  val producer = new KafkaProducer[String, String](props)

  consumer.subscribe(List(topicName).asJava)

  def consume: List[String] = {
    consumer.poll(10000)
      .records(topicName)
      .iterator()
      .asScala
      .toList
      .map(_.value())
  }

  def send(msg: String): Future[RecordMetadata] = {
    producer.send(new ProducerRecord[String, String](topicName, msg))
  }
}