package services

import io.circe
import io.circe.Decoder
import io.circe.parser.decode

class DecoderService {
  def decodeTo[T: Decoder](json: String): Either[circe.Error, T] = {
    decode[T](json)
  }
}
